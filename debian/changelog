python-zstd (1.5.5.1-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 1.5.5.1
  * use new dh-sequence-python3
  * build with external libzstd (Closes: #1039114)
  * remove extraneous build dep on python3-mock
  * clean cache files

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 25 Feb 2024 16:26:24 +0100

python-zstd (1.5.2.5-1) unstable; urgency=medium

  * Drop unnecessary dependency on build-essential.
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 09:27:49 +0100

python-zstd (1.5.0.2-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * New upstream release.
    - Supports Python 3.10 (Closes: 999395)
  * Bump copyright years, and collapse common groups.
  * Update standards version to 4.6.0, no changes needed.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 20 Nov 2021 08:28:30 -0400

python-zstd (1.4.5.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 21 Aug 2021 14:14:17 +0100

python-zstd (1.4.5.1-2) unstable; urgency=medium

  * Uploading source-only.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Oct 2020 14:58:01 +0200

python-zstd (1.4.5.1-1) unstable; urgency=low

  [ Chris MacNaughton & Thomas Goirand]
  * Initial release. (Closes: #970270)

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Sep 2020 10:08:15 +0200
